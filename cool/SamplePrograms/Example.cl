Class Main {  
    main() : Object {
        {
         -- Se realiza la suma y se imprime el resultado
         (new ClaseHijo).sumar(5, 5);
         
         -- Recibe un parametro, si es igual a 1 imprime Es igual caso contrario imprime No es igual
         (new ClaseHijo).metodoIfEq(1);
         (new ClaseHijo).metodoIfEq(2);
         
         -- Recibe un string e imprime 3 veces ese string
         (new ClaseHijo).metodoWhile("Prueba while");
         
         -- En la clase hijo se llama al metodo de la clase padre
         (new ClaseHijo).llamarMetodoPadre();
         
         -- Recibe un boolean, imprime Es true si el parametro es true caso contrario imprime Es false 
         (new ClaseHijo).metodoIf(false);
         
         -- Se realiza varias operaciones para probar la precedencia
         (new ClaseHijo).metodoPruebaPrecedencia();
         
         -- Se llama directamente al metodo desde la clase padre
         (new ClaseHijo)@ClasePadre.metodoPadre();
        }
    };
};

Class ClasePadre inherits IO {

	metodoPadre():String {
		{
			out_string("Metodo de la clase padre");
			"FIN METODO PADRE";
		}
	};
};

Class ClaseHijo inherits ClasePadre {
	sec:ClaseSecundaria <- (new ClaseSecundaria);
	n:Int <- 3;
	i:Int <- 0;
	
	sumar(a1:Int, a2:Int):Int {
		{
			a1 + a2;
			out_int((a1 + a2));
		}
	};

	metodoIf(cond:Bool):String {
		{
			if cond then
				out_string("Es True")
			else
				out_string("Es false")
			fi;
			"FIN METODO IF";
		}	
	};

	metodoIfEq(cond:Int):String {
		 { 
		 	if(cond = 1) then
				out_string("Es igual")
			else
				out_string("No es igual")
			fi;
			"FIN METODO IF EQUAL";
		}
	};
	
	metodoWhile(str:String):String {
		{
			while i < n loop
				{
					out_string(str);
					i <- i + 1;
				}
			pool;
			"FIN METODO WHILE";
		}
	};

	llamarMetodoPadre():String {
		{
			metodoPadre();
			"FIN LLAMAR A METODO PADRE";
		}
	};

	metodoPruebaPrecedencia():Int {
		{
		  out_int((2*15-5));
		  1;
		}
	};

	getSecundario():ClaseSecundaria {
		(new ClaseSecundaria)
	};
};

Class ClaseSecundaria inherits IO {
	metodoPropio():Int {
		{
			metodoReturnInt();
		}
	};

	metodoReturnInt():Int {
		{
			1;
		}
	};
};