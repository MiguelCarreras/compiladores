*target*
*.jar
*.war
*.ear
*.class

# eclipse specific git ignore
*.pydevproject
.project
.metadata
bin/**
tmp/**
build/**
tmp/**/*
*.tmp
*.bak
*.swp
*~.nib
local.properties
.classpath
.settings/
.loadpath

# External tool builders
.externalToolBuilders/

src/Parser/parser.java
src/Parser/sym.java

src/Scanner/scanner.java

# Locally stored "Eclipse launch configurations"
*.launch
/bin/
