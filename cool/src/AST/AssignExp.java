package AST;
import AST.Visitor.Visitor;

public class AssignExp extends Exp {
  public Identifier i;
  public Exp e;
  
  public AssignExp(Identifier ai, Exp ae1, int ln) {
    super(ln);
    e=ae1; i=ai;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}
