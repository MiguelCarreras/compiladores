package AST;
import AST.Visitor.Visitor;

public class BlockExp extends Exp {
  public BlockList bl;
  
  public BlockExp(BlockList ae1, int ln) {
    super(ln);
    bl=ae1;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}
