package AST;
import AST.Visitor.Visitor;

public class CaseExp extends Exp {
  public Exp e;
  public CaseOptList col;
  
  public CaseExp(Exp ae, CaseOptList acol, int ln) {
    super(ln);
    e=ae; col=acol;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}