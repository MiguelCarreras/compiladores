package AST;
import AST.Visitor.Visitor;

public class CaseOptExp extends Exp {
  public Identifier i;
  public IdentifierType it;
  public Exp e;
  
  public CaseOptExp(Identifier ai, IdentifierType ait, Exp ae, int ln) {
    super(ln);
    e=ae; i=ai; it=ait;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}
