package AST;
import AST.Visitor.Visitor;

public class ClassDeclInherits extends ClassDecl {
  public IdentifierType i;
  public IdentifierType j;
  public FeatureList fl;  
 
  public ClassDeclInherits(IdentifierType ai, IdentifierType aj, FeatureList afl, int ln) {
    super(ln);
    i=ai; j=aj; fl=afl;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}
