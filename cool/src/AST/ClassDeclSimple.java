package AST;
import AST.Visitor.Visitor;

public class ClassDeclSimple extends ClassDecl {
  public IdentifierType i;
  //public VarDeclList vl;  
  public FeatureList fl;
  //public MethodDeclList ml;
 
  public ClassDeclSimple(IdentifierType ai, FeatureList afl, int ln) {
    super(ln);
    i=ai; fl=afl;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}
