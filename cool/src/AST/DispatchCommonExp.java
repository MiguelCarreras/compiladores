package AST;
import AST.Visitor.Visitor;

public class DispatchCommonExp extends Exp {
  public Identifier i;
  public Exp e;
  public ExpList el;
  
  public DispatchCommonExp(Exp ae, Identifier ai, ExpList ael, int ln) {
    super(ln);
    e=ae; i=ai; el=ael;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}
