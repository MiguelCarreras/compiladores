package AST;
import AST.Visitor.Visitor;

public class DispatchLongExp extends Exp {
  public Identifier i;
  public IdentifierType it;
  public Exp e;
  public ExpList el;
  
  public DispatchLongExp(Identifier ai, IdentifierType ait, Exp ae,  ExpList ael, int ln) {
    super(ln);
    e=ae; i=ai; el=ael; it=ait;
  }

  public DispatchLongExp(Identifier ai, Exp ae, ExpList ael, int ln) {
	  super(ln);
	  e=ae; i=ai; el=ael; it=null;
  }
  
  public void accept(Visitor v) {
    v.visit(this);
  }
}