package AST;
import AST.Visitor.Visitor;

public class DispatchShortExp extends Exp {
  public Identifier i;
  public ExpList el;
  
  public DispatchShortExp(ExpList ael, Identifier ai, int ln) {
    super(ln);
    el=ael; i=ai;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}
