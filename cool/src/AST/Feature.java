package AST;
import AST.Visitor.Visitor;

public abstract class Feature extends ASTNode {
    public Feature(int ln) {
        super(ln);
    }
    public abstract void accept(Visitor v);
}
