package AST;

import java.util.List;
import java.util.ArrayList;

public class FeatureList extends ASTNode {
   private List<Feature> list;

   public FeatureList(int ln) {
      super(ln);
      list = new ArrayList<Feature>();
   }

   public void add(Feature n) {
      list.add(n);
   }

   public Feature get(int i)  { 
      return list.get(i); 
   }

   public int size() { 
      return list.size(); 
   }
}
