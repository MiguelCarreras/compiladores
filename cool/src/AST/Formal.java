package AST;
import AST.Visitor.Visitor;

public class Formal extends ASTNode{
  public IdentifierType t;
  public Identifier i;
 
  public Formal(IdentifierType at, Identifier ai, int ln) {
    super(ln);
    t=at; i=ai;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}
