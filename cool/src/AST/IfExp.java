package AST;
import AST.Visitor.Visitor;

public class IfExp extends Exp {
  public Identifier i;
  public Exp e1, e2, e3;
  
  public IfExp(Exp ae1, Exp ae2, Exp ae3, int ln) {
    super(ln);
    e1=ae1; e2=ae2; e3=ae3;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}