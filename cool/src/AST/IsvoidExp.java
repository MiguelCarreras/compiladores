package AST;
import AST.Visitor.Visitor;

public class IsvoidExp extends Exp {
  public Exp e;
  
  public IsvoidExp(Exp ae1, int ln) {
    super(ln);
    e=ae1;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}
