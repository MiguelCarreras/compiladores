package AST;
import AST.Visitor.Visitor;

public class LetExp extends Exp {
  public Exp e;
  public LetOptList lol;
  
  public LetExp(Exp ae, LetOptList alol, int ln) {
    super(ln);
    e=ae; lol=alol;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}