package AST;
import AST.Visitor.Visitor;

public class LetLongOptExp extends Exp {
  public Identifier i;
  public IdentifierType it;
  public Exp e;
	
  public LetLongOptExp(Identifier ai, IdentifierType ait, Exp ae, int ln) {
    super(ln);
    i=ai; it=ait; e=ae;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}