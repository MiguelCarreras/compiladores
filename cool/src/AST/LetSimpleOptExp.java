package AST;
import AST.Visitor.Visitor;

public class LetSimpleOptExp extends Exp {
  public Identifier i;
  public IdentifierType it;
  
  public LetSimpleOptExp(Identifier ai, IdentifierType ait, int ln) {
    super(ln);
    i=ai; it=ait;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}
