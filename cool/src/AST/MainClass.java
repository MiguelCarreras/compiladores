package AST;
import AST.Visitor.Visitor;

public class MainClass extends ASTNode{
  public Identifier i1,i2;
  public Feature s;

  public MainClass(Identifier ai1, Identifier ai2, Feature as, int ln) {
    super(ln);
    i1=ai1; i2=ai2; s=as;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}

