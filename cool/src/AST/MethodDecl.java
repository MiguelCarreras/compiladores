package AST;
import AST.Visitor.Visitor;

public class MethodDecl extends Feature {
  public IdentifierType t;
  public Identifier i;
  public FormalList fl;
  public Exp e;

  public MethodDecl(IdentifierType at, Identifier ai, FormalList afl, Exp ae, int ln) {
    super(ln);
    t=at; i=ai; fl=afl; e=ae;
  }
 
  public void accept(Visitor v) {
    v.visit(this);
  }
}
