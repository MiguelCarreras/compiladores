package AST;
import AST.Visitor.Visitor;

public class NewExp extends Exp {
  public IdentifierType it;
  
  public NewExp(IdentifierType ait, int ln) {
    super(ln);
    it=ait;
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}