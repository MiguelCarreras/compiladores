package AST;
import AST.Visitor.Visitor;

public class Program extends ASTNode {
  //public MainClass m;
  public ClassDeclList cl;

  public Program(ClassDeclList acl, int ln) {
    super(ln);
    cl=acl; 
  }

  public void accept(Visitor v) {
    v.visit(this);
  }
}
