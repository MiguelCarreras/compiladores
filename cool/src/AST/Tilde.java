package AST;
import AST.Visitor.Visitor;

public class Tilde extends Exp {
	public Exp e1;
	
  public Tilde(Exp ae, int ln) {
	  super(ln);
	  e1 = ae;
  }
  public void accept(Visitor v) {
    v.visit(this);
  }
}
