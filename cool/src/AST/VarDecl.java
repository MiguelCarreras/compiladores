package AST;
import AST.Visitor.Visitor;

public class VarDecl extends Feature	 {
  public IdentifierType t;
  public Identifier i;
  public Exp e;
  
  public VarDecl(IdentifierType at, Identifier ai, Exp ae, int ln) {
	  super(ln);
	  t=at; i=ai; e=ae;
  }
  
  public VarDecl(IdentifierType at, Identifier ai, int ln) {
    super(ln);
    t=at; i=ai; e=null;
  }
  
  public void accept(Visitor v) {
    v.visit(this);
  }
}
