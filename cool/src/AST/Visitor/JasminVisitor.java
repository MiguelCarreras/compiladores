package AST.Visitor;

import java.util.ArrayList;
import java.util.List;

import AST.And;
import AST.AssignExp;
import AST.BlockExp;
import AST.CaseExp;
import AST.CaseOptExp;
import AST.ClassDeclInherits;
import AST.ClassDeclSimple;
import AST.DispatchCommonExp;
import AST.DispatchLongExp;
import AST.DispatchShortExp;
import AST.Display;
import AST.Divide;
import AST.Equal;
import AST.False;
import AST.Formal;
import AST.Identifier;
import AST.IdentifierExp;
import AST.IdentifierType;
import AST.IfExp;
import AST.IntegerLiteral;
import AST.IntegerType;
import AST.IsvoidExp;
import AST.LessThan;
import AST.LessThanOrEqual;
import AST.LetExp;
import AST.LetLongOptExp;
import AST.LetSimpleOptExp;
import AST.MainClass;
import AST.MethodDecl;
import AST.Minus;
import AST.Multiply;
import AST.NewExp;
import AST.Not;
import AST.Plus;
import AST.Print;
import AST.Program;
import AST.StringLiteral;
import AST.This;
import AST.Tilde;
import AST.True;
import AST.VarDecl;
import AST.WhileExp;
import JasminCodeGenerator.JasminCodeGenerator;
import Semantic.Symbols.ClassSymbol;
import Semantic.Symbols.MethodSymbol;
import Semantic.Symbols.ParameterSymbol;
import Semantic.Symbols.VariableSymbol;
import Semantic.TableSymbol.ClassTable;
import Semantic.TableSymbol.MethodTable;
import Semantic.TableSymbol.TableSymbol;

public class JasminVisitor implements Visitor{

	private TableSymbol tableSymbol;
	private ClassSymbol currentClass;
	private MethodTable currentMethodT;
	private JasminCodeGenerator jasminGenerator;
	private Boolean isVar = false;
	private String currentVar;
	private String currentCl;
	private List<String> newObjectCode = new ArrayList<String>();
	private Boolean hasConst = false;
	
	public JasminVisitor(String fileDir, TableSymbol table) {
		tableSymbol = table;
		jasminGenerator = new JasminCodeGenerator(fileDir);
	}
	
	@Override
	public void visit(Display n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Program n) {
		// TODO Auto-generated method stub
		jasminGenerator.clearDirectory();
		for (int i = 0; i < n.cl.size(); i++) {
			n.cl.get(i).accept(this);
		}
	}

	@Override
	public void visit(MainClass n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ClassDeclSimple n) {
		// TODO Auto-generated method stub
		String className = n.i.s;
		currentClass = new ClassSymbol(className, n.i.getLine());
		//MethodSymbol method = MethodSymbol("main", "void", n.i.getLine());
		jasminGenerator.classSchedule(className);
		jasminGenerator.incrementStack();
		for( int i = 0; i < n.fl.size(); i++) {
			n.fl.get(i).accept(this);
		}
		jasminGenerator.generateJasminFile(className);// Genera el archivo jasmin
		hasConst = false;
	}

	@Override
	public void visit(ClassDeclInherits n) {
		// TODO Auto-generated method stub
		String className = n.i.s;
		String inheritClassName = n.j.s;
		if(n.j.s.equals("IO")) inheritClassName = "java/lang/Object";
		ClassSymbol inheritClass = new ClassSymbol(inheritClassName, n.j.getLine());
		currentClass = new ClassSymbol(className, inheritClass, n.i.getLine());
		jasminGenerator.classInheritSchedule(className, inheritClassName);
		jasminGenerator.incrementStack();
		for( int i = 0; i < n.fl.size(); i++) {
			n.fl.get(i).accept(this);
		}
		jasminGenerator.generateJasminFile(className);// Genera el archivo jasmin
		hasConst = false;
	}

	@Override
	public void visit(VarDecl n) {
		// TODO Auto-generated method stub
		String varName = n.i.s;
		currentVar = varName;
		String varClassName = jasminGenerator.defineJasminClass(n.t.s);
		jasminGenerator.varDeclarationSchedule(varName, varClassName);
		if(n.e != null) {
			isVar = true;
			n.e.accept(this);
		}
	}

	@Override
	public void visit(MethodDecl n) {
		// TODO Auto-generated method stub
		if(hasConst == false) {
			jasminGenerator.defaultConstructor(newObjectCode, currentClass.getClassInheritName());
			newObjectCode.clear();
			hasConst = true;
		}
		n.i.accept(this);
		MethodTable methodTable = tableSymbol.getMethodTableOfClassTable(currentClass.getId(), n.i.s);
		currentMethodT = methodTable;
		if(methodTable.getMethodName().equals("main")) {
			jasminGenerator.methodMainInitSchedule();
			n.t.accept(this);
			n.e.accept(this);
			jasminGenerator.methodMainEndSchedule();
		}else {
			jasminGenerator.methodInitSchedule(methodTable, n.fl);
			n.t.accept(this);
			n.e.accept(this);
			jasminGenerator.methodEndSchedule(methodTable.getReturnedMethodClassName());
		}
	}

	@Override
	public void visit(Formal n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IntegerType n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IdentifierType n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Print n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(AssignExp n) {
		// TODO Auto-generated method stub
		String varName = n.i.s;
		ClassTable classTable = tableSymbol.getClassTable(currentClass.getId());
		VariableSymbol var;
		if(classTable.getVariable(varName) != null) {
			n.e.accept(this);
			var = classTable.getVariable(varName);
			String varType = jasminGenerator.defineJasminClass(var.getType());
			jasminGenerator.addListCode("\tputfield "+classTable.getClassName()+"/"+varName+" "+varType);
		} else {
			n.e.accept(this);
			var = classTable.getVariable(varName);
			String inst = null;
			if(var.getType().equals("Int")) {
				inst = "istore";
			} else {
				inst = "astore";
			}
			jasminGenerator.addListCode("\t"+inst+"_"+n.getLine());
		}
	}

	@Override
	public void visit(DispatchShortExp n) {
		// TODO Auto-generated method stub
		MethodSymbol aux = new MethodSymbol(n.i.s, null, n.getLine());
		if(n.i.s.equals("out_string")) {
			currentMethodT = new MethodTable(aux);
			jasminGenerator.addListCode("\tgetstatic java/lang/System/out Ljava/io/PrintStream;");
			n.el.get(0).accept(this);
			jasminGenerator.addListCode("\tinvokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		}else if(n.i.s.equals("out_int")) {
			currentMethodT = new MethodTable(aux);
			jasminGenerator.addListCode("\tgetstatic java/lang/System/out Ljava/io/PrintStream;");
			n.el.get(0).accept(this);
			jasminGenerator.addListCode("\tinvokevirtual java/io/PrintStream/println(I)V"); 
		}else {
			ClassTable classTable = tableSymbol.getClassTable(currentClass.getId());
			
			for( int i = 0; i < n.el.size(); i++) {
				n.el.get(i).accept(this);
			}
			
			currentCl = currentClass.getId();
			ClassTable classT = tableSymbol.getClassTable(currentClass.getId());
			
			MethodTable methodT = classT.getMethodTable(n.i.s);
			//Si no encuentra el metodo lo busca en su padre
			while(methodT == null) {
				ClassTable auxClassT = tableSymbol.getClassTable(classT.getClassInheritName());
				methodT = auxClassT.getMethodTable(n.i.s);
			}
			currentMethodT = methodT;
			String jCode = "\tinvokevirtual "+currentCl+"/"+methodT.getMethodName()+"(";
			for(String key: methodT.getParameters().keySet()) {				
				jCode+= jasminGenerator.defineJasminClass(methodT.getParameters().get(key).getParameterClassName());
			}	
			jCode += ")"+jasminGenerator.defineJasminClass(methodT.getReturnedMethodClassName());
			jasminGenerator.addListCode("\taload_0");
			jasminGenerator.addListCode(jCode);		
		}
	}

	@Override
	public void visit(DispatchCommonExp n) {
		
		MethodSymbol aux = new MethodSymbol(n.i.s, null, n.getLine());
		if(n.i.s.equals("out_string")) {
			currentMethodT = new MethodTable(aux);
			jasminGenerator.addListCode("\tgetstatic java/lang/System/out Ljava/io/PrintStream;");
			n.el.get(0).accept(this);
			jasminGenerator.addListCode("\tinvokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		}else if(n.i.s.equals("out_int")) {
			currentMethodT = new MethodTable(aux);
			jasminGenerator.addListCode("\tgetstatic java/lang/System/out Ljava/io/PrintStream;");
			n.el.get(0).accept(this);
			jasminGenerator.addListCode("\tinvokevirtual java/io/PrintStream/println(I)V"); 
		}else {
			ClassTable classTable = tableSymbol.getClassTable(currentClass.getId());
			n.e.accept(this);
			
			for( int i = 0; i < n.el.size(); i++) {
				n.el.get(i).accept(this);
			}
			
			if(currentVar != null) {
				VariableSymbol var = classTable.getVariable(currentVar);
				ClassTable classT = tableSymbol.getClassTable(var.getType());
				MethodTable methodT = classT.getMethodTable(n.i.s);
				while(methodT == null) {
					ClassTable auxClassT = tableSymbol.getClassTable(classT.getClassInheritName());
					methodT = auxClassT.getMethodTable(n.i.s);
				}
				currentMethodT = methodT;
				String jCode = "\tinvokevirtual "+var.getType()+"/"+methodT.getMethodName()+"(";
				jCode += ")"+jasminGenerator.defineJasminClass(methodT.getReturnedMethodClassName());
				jasminGenerator.addListCode(jCode);
			} else {
				ClassTable classT = tableSymbol.getClassTable(currentCl);
				MethodTable methodT = classT.getMethodTable(n.i.s);
				currentMethodT = methodT;
				while(methodT == null) {
					ClassTable auxClassT = tableSymbol.getClassTable(classT.getClassInheritName());
					methodT = auxClassT.getMethodTable(n.i.s);
				}
				String jCode = "\tinvokevirtual "+currentCl+"/"+methodT.getMethodName()+"(";
				for(String key: methodT.getParameters().keySet()) {
					
					jCode+= jasminGenerator.defineJasminClass(methodT.getParameters().get(key).getParameterClassName());
				}	
				jCode += ")"+jasminGenerator.defineJasminClass(methodT.getReturnedMethodClassName());
				jasminGenerator.addListCode(jCode);
			}
		}
	}

	@Override
	public void visit(DispatchLongExp n) {
		// TODO Auto-generated method stub
		MethodSymbol aux = new MethodSymbol(n.i.s, null, n.getLine());
		if(n.i.s.equals("out_string")) {
			currentMethodT = new MethodTable(aux);
			jasminGenerator.addListCode("\tgetstatic java/lang/System/out Ljava/io/PrintStream;");
			n.el.get(0).accept(this);
			jasminGenerator.addListCode("\tinvokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		}else if(n.i.s.equals("out_int")) {
			currentMethodT = new MethodTable(aux);
			jasminGenerator.addListCode("\tgetstatic java/lang/System/out Ljava/io/PrintStream;");
			n.el.get(0).accept(this);
			jasminGenerator.addListCode("\tinvokevirtual java/io/PrintStream/println(I)V"); 
		}else {
			ClassTable classTable = tableSymbol.getClassTable(n.it.s);
			n.e.accept(this);
			
			for( int i = 0; i < n.el.size(); i++) {
				n.el.get(i).accept(this);
			}
			
			if(currentVar != null) {
				VariableSymbol var = classTable.getVariable(currentVar);
				ClassTable classT = tableSymbol.getClassTable(var.getType());
				MethodTable methodT = classT.getMethodTable(n.i.s);
				while(methodT == null) {
					ClassTable auxClassT = tableSymbol.getClassTable(classT.getClassInheritName());
					methodT = auxClassT.getMethodTable(n.i.s);
				}
				currentMethodT = methodT;
				String jCode = "\tinvokevirtual "+var.getType()+"/"+methodT.getMethodName()+"(";
				jCode += ")"+jasminGenerator.defineJasminClass(methodT.getReturnedMethodClassName());
				jasminGenerator.addListCode(jCode);
			} else {
				ClassTable classT = tableSymbol.getClassTable(currentCl);
				MethodTable methodT = classT.getMethodTable(n.i.s);
				currentMethodT = methodT;
				while(methodT == null) {
					ClassTable auxClassT = tableSymbol.getClassTable(classT.getClassInheritName());
					methodT = auxClassT.getMethodTable(n.i.s);
				}
				String jCode = "\tinvokevirtual "+currentCl+"/"+methodT.getMethodName()+"(";
				for(String key: methodT.getParameters().keySet()) {
					
					jCode+= jasminGenerator.defineJasminClass(methodT.getParameters().get(key).getParameterClassName());
				}	
				jCode += ")"+jasminGenerator.defineJasminClass(methodT.getReturnedMethodClassName());
				jasminGenerator.addListCode(jCode);
			}
		}

	}

	@Override
	public void visit(WhileExp n) {
		// TODO Auto-generated method stub
		String whileL = jasminGenerator.jasminLabel("while");
		String whileEndL = jasminGenerator.jasminLabel("while_end");
		String trueL = jasminGenerator.jasminLabel("true");
		jasminGenerator.newLabel(whileL);
		n.e1.accept(this);
		String ifEq = "ifeq "+trueL;
		jasminGenerator.addListCode("\t"+ifEq);
		jasminGenerator.addListCode("\tgoto "+whileEndL);
		jasminGenerator.newLabel(trueL);
		jasminGenerator.addListCode("\taload_0");
		n.e2.accept(this);
		jasminGenerator.addListCode("\tgoto "+whileL);
		
		jasminGenerator.newLabel(whileEndL);
	}

	@Override
	public void visit(IfExp n) {
		// TODO Auto-generated method stub
		n.e1.accept(this);
		String trueL = jasminGenerator.jasminLabel("true");
		String falseL = jasminGenerator.jasminLabel("false");
		String ifEq = "ifeq "+trueL;
		jasminGenerator.addListCode("\t"+ifEq);
		n.e3.accept(this);
		jasminGenerator.addListCode("\tgoto "+falseL);
		jasminGenerator.newLabel(trueL);
		n.e2.accept(this);
		jasminGenerator.newLabel(falseL);
	}

	@Override
	public void visit(BlockExp n) {
		// TODO Auto-generated method stub
		for ( int i = 0; i < n.bl.size(); i++ ) {
			n.bl.get(i).accept(this);
		}	
	}

	@Override
	public void visit(CaseOptExp n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(CaseExp n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(LetSimpleOptExp n) {
		// TODO Auto-generated method stub
	}

	@Override
	public void visit(LetLongOptExp n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(LetExp n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(NewExp n) {
		// TODO Auto-generated method stub
		String className = n.it.s;
		if(isVar == true) {
			newObjectCode.add("\tnew "+className);
			newObjectCode.add("\tdup");
			newObjectCode.add("\tinvokespecial "+className+"/<init>()V");
			newObjectCode.add("\tputfield "+currentClass.getId()+"/"+currentVar+" "+jasminGenerator.defineJasminClass(className));
			isVar = false;
		} else {
			currentCl = className;
			jasminGenerator.addListCode("\n\tnew "+className);
			jasminGenerator.addListCode("\tdup");
			jasminGenerator.addListCode("\tinvokespecial "+className+"/<init>()V\n");
		}
		
		n.it.accept(this);
	}

	@Override
	public void visit(And n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(LessThan n) {
		// TODO Auto-generated method stub
		n.e1.accept(this);
		n.e2.accept(this);
		jasminGenerator.lessThanLabel(n);
	}

	@Override
	public void visit(IsvoidExp n) {
		// TODO Auto-generated method stub
	}

	@Override
	public void visit(LessThanOrEqual n) {
		// TODO Auto-generated method stub
		n.e1.accept(this);
		n.e2.accept(this);
		jasminGenerator.lessThanOrEqLabel(n);
	}

	@Override
	public void visit(Equal n) {
		// TODO Auto-generated method stub
		n.e1.accept(this);
		n.e2.accept(this);
		jasminGenerator.equalLabel(n);
	}

	@Override
	public void visit(Not n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(True n) {
		// TODO Auto-generated method stub
		jasminGenerator.incrementStack();
		jasminGenerator.addListCode("\t"+jasminGenerator.defineComand("iconst", 0));
	}

	@Override
	public void visit(False n) {
		// TODO Auto-generated method stub
		jasminGenerator.incrementStack();
		jasminGenerator.addListCode("\t"+jasminGenerator.defineComand("iconst", 1));
	}

	@Override
	public void visit(Tilde n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Plus n) {
		// TODO Auto-generated method stub
		n.e1.accept(this);
		n.e2.accept(this);
		jasminGenerator.addListCode("\tiadd");
	}

	@Override
	public void visit(Minus n) {
		// TODO Auto-generated method stub
		n.e1.accept(this);
		n.e2.accept(this);
		jasminGenerator.addListCode("\tisub");
	}

	@Override
	public void visit(Multiply n) {
		// TODO Auto-generated method stub
		n.e1.accept(this);
		n.e2.accept(this);
		jasminGenerator.addListCode("\timul");
	}

	@Override
	public void visit(Divide n) {
		// TODO Auto-generated method stub
		n.e1.accept(this);
		n.e2.accept(this);
		jasminGenerator.addListCode("\tidiv");
	}

	@Override
	public void visit(IntegerLiteral n) {
		// TODO Auto-generated method stub
		if(isVar == true) {
			String code = jasminGenerator.getLastListCode();
			code += " = "+n.i;
			jasminGenerator.addListCodeToLast(code);
			isVar = false;
		}else {
			jasminGenerator.incrementStack();
			jasminGenerator.addListCode("\n\t;Integer Literal "+n.i);
			jasminGenerator.addListCode("\tldc "+ n.i);
		}
		
	}

	@Override
	public void visit(StringLiteral n) {
		// TODO Auto-generated method stub
		if(isVar == true) {
			String code = jasminGenerator.getLastListCode();
			code += " = "+n.s;
			jasminGenerator.addListCodeToLast(code);
			isVar = false;
		}else {
			jasminGenerator.incrementStack();
			jasminGenerator.addListCode("\n\t;String Literal "+n.s);
			jasminGenerator.addListCode("\tldc "+ n.s);
		}
	}

	@Override
	public void visit(IdentifierExp n) {
		// TODO Auto-generated method stub
		ClassTable classTable = tableSymbol.getClassTable(currentClass.getId());
		if(classTable.getVariable(n.s) != null) {
			String varName = n.s;
			currentVar = varName;
			VariableSymbol var = classTable.getVariable(varName);
			String varType = jasminGenerator.defineJasminClass(var.getType());
			jasminGenerator.addListCode("\taload_0");
			jasminGenerator.addListCode("\tgetfield "+classTable.getClassName()+"/"+var.getId()+" "+varType);
		} else {
			String paramClass;
			int paramIndex;
			if(currentMethodT.getMethodName().equals("out_string") || currentMethodT.getMethodName().equals("out_int")) {
				paramIndex = 1;
				if(currentMethodT.getMethodName().equals("out_string")) {
					paramClass = "String";
				}else {
					paramClass = "Int";
				}
			}else {
				ParameterSymbol param = currentMethodT.getParameter(n.s);
				paramClass = param.getParameterClassName();
				paramIndex = currentMethodT.getParameterIndex(n.s);
			}
			String jParamCode = "\t"+jasminGenerator.defineJasminLoad(paramClass)+" "+paramIndex;
			jasminGenerator.addListCode(jParamCode);
		}
		currentVar = null;
	}

	@Override
	public void visit(This n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Identifier n) {
		// TODO Auto-generated method stub
		
	}
	
}
