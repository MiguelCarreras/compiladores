package AST.Visitor;

import AST.*;
//Sample print visitor modified from MiniJava project for the Cool Language
// Sample print visitor from MiniJava web site with small modifications for UW CSE.
// HP 10/11

public class PrettyPrintVisitor implements Visitor {

  // Display added for toy example language.  Not used in regular MiniJ
  public void visit(Display n) {
    System.out.print("display ");
    n.e.accept(this);
    System.out.print(";");
  }
  
  // MainClass m;
  // ClassDeclList cl;
  public void visit(Program n) {
    //n.m.accept(this);
    for ( int i = 0; i < n.cl.size(); i++ ) {
        System.out.println();
        n.cl.get(i).accept(this);
    }
  }
  
  // Identifier i1,i2;
  // Statement s;
  public void visit(MainClass n) {
    System.out.print("class ");
    n.i1.accept(this);
    System.out.println(" {");
    System.out.print("  public static void main (String [] ");
    n.i2.accept(this);
    System.out.println(") {");
    System.out.print("    ");
    n.s.accept(this);
    System.out.println("  }");
    System.out.println("}");
  }

  // Identifier i;
  // VarDeclList vl;
  // MethodDeclList ml;
  public void visit(ClassDeclSimple n) {
    System.out.print("class ");
    n.i.accept(this);
    System.out.println(" { ");
    for (int i = 0; i < n.fl.size(); i++) {
		System.out.print("  ");
		n.fl.get(i).accept(this);
		if (i + 1 < n.fl.size()) {
			System.out.println();
		}
	}
/*
    for ( int i = 0; i < n.ml.size(); i++ ) {
        System.out.println();
        n.ml.get(i).accept(this);
    }*/
    System.out.println();
    System.out.println("};");
  }
 
  // Identifier i;
  // Identifier j;
  // VarDeclList vl;
  // MethodDeclList ml;
  public void visit(ClassDeclInherits n) {
    System.out.print("class ");
    n.i.accept(this);
    System.out.println(" inherits ");
    n.j.accept(this);
    System.out.println(" { ");
    /*for ( int i = 0; i < n.vl.size(); i++ ) {
        System.out.print("  ");
        n.vl.get(i).accept(this);
        if ( i+1 < n.vl.size() ) { System.out.println(); }
    }
    for ( int i = 0; i < n.ml.size(); i++ ) {
        System.out.println();
        n.ml.get(i).accept(this);
    }*/
    for (int i = 0; i < n.fl.size(); i++) {
		System.out.print("  ");
		n.fl.get(i).accept(this);
		if (i + 1 < n.fl.size()) {
			System.out.println();
		}
	}
    System.out.println();
    System.out.println("};");
  }

  // Type t;
  // Identifier i;
  public void visit(VarDecl n) {
    n.t.accept(this);
    System.out.print(" ");
    n.i.accept(this);
    if(n.e != null) {
    	System.out.print(" <- ");
    	n.e.accept(this);
    }
    System.out.print(";");
  }
  
  // Type t;
  // Identifier i;
  // FormalList fl;
  // VarDeclList vl;
  // StatementList sl;
  // Exp e;
  public void visit(MethodDecl n) {
	  //System.out.print(" ");
	 n.i.accept(this);
	 System.out.print(" (");
	 for ( int i = 0; i < n.fl.size(); i++ ) {
		 n.fl.get(i).accept(this);
	     if (i+1 < n.fl.size()) { System.out.print(", "); }
	 }
	 System.out.print("): ");
	 n.t.accept(this);
	 System.out.print(" ");
	 System.out.println("{");
	 System.out.print("\t");
	 n.e.accept(this);
	 System.out.println();
	 System.out.print("  };");
  }

  // Type t;
  // Identifier i;
  public void visit(Formal n) {
    n.t.accept(this);
    System.out.print(" ");
    n.i.accept(this);
  }


  public void visit(IntegerType n) {
    System.out.print("int");
  }

  // String s;
  public void visit(IdentifierType n) {
    System.out.print(n.s);
  }


//Exp e;
 // Statement s1,s2;
 public void visit(IfExp n) {
   System.out.print("if ");
   System.out.println();
   System.out.print("      ");
   n.e1.accept(this);
   System.out.println();
   System.out.print("    then");
   System.out.println();
   System.out.print("      ");
   n.e2.accept(this);
   System.out.println();
   System.out.print("    else");
   System.out.println();
   System.out.print("      ");
   n.e3.accept(this);
   System.out.println();
   System.out.print("    fi");
 }
  
 public void visit(DispatchCommonExp n) {
   n.e.accept(this);
   System.out.print(".");
   n.i.accept(this);
   System.out.print("( ");
   for ( int i = 0; i < n.el.size(); i++ ) {
       n.el.get(i).accept(this);
       if (i+1 < n.el.size()) { System.out.print(", "); }
   }
   System.out.print(" )");
 }
 
 public void visit(DispatchShortExp n) {
   n.i.accept(this);
   System.out.print("( ");
   for ( int i = 0; i < n.el.size(); i++ ) {
     n.el.get(i).accept(this);
	 if (i+1 < n.el.size()) { System.out.print(", "); }
   }
   System.out.print(" )");	 	
 }
 
 public void visit(DispatchLongExp n) {
	 n.e.accept(this);
	 if(n.it != null) {
		 System.out.print("@");
		 n.it.accept(this); 
	 }
	 System.out.print(".");
	 n.i.accept(this);
	 System.out.print("( ");
	 for ( int i = 0; i < n.el.size(); i++ ) {
	     n.el.get(i).accept(this);
	     if (i+1 < n.el.size()) { System.out.print(", "); }
	 }
	 System.out.print(" )"); 	
 }
 
 public void visit(WhileExp n) {
	 System.out.print("While ");
	   System.out.println();
	   System.out.print("      ");
	   n.e1.accept(this);
	   System.out.println();
	   System.out.print("    loop");
	   System.out.println();
	   System.out.print("      ");
	   n.e2.accept(this);
	   System.out.println();
	   System.out.print("    pool"); 	
 }
 
 public void visit(BlockExp n) {
	 System.out.print("{ ");
	   System.out.println();
	   System.out.print("      ");
	   for ( int i = 0; i < n.bl.size(); i++ ) {
		     n.bl.get(i).accept(this);
		     if (i < n.bl.size()) { System.out.print("; "); }
		 }	   
	   System.out.println();
	   System.out.print("    }");
 }
 
  public void visit(CaseOptExp n) {
	 n.i.accept(this);
	 System.out.print(" : ");
	 n.it.accept(this);
	 System.out.print(" => ");
	 n.e.accept(this);
  }
  
  public void visit(CaseExp n) {
	  System.out.print("case ");
	  n.e.accept(this);
	  System.out.print(" of");
	  System.out.println();
	  for ( int i = 0; i < n.col.size(); i++ ) {
		  	System.out.print("       ");
		     n.col.get(i).accept(this);
		     if (i < n.col.size()) { System.out.print("; "); }
		     System.out.println();
	  }
	  System.out.print("    esac");
  }
  
  public void visit(LetSimpleOptExp n) {
	  n.i.accept(this);
	  System.out.print(" : ");
	  n.it.accept(this);
  }
  
  public void visit(LetLongOptExp n) {
	  n.i.accept(this);
	  System.out.print(" : ");
	  n.it.accept(this);
	  System.out.print(" <- ");
	  n.e.accept(this);
  }
  
  public void visit(LetExp n) {
	  System.out.print("let ");
	  for ( int i = 0; i < n.lol.size(); i++ ) {
		  n.lol.get(i).accept(this);
		  if (i < n.lol.size()) { System.out.print(", "); }
	  }
	  System.out.print(" in ");
	  n.e.accept(this);
  }
  
  public void visit(NewExp n) {
	  System.out.print("new ");
	  n.it.accept(this);
  }

  // Exp e;
  public void visit(Print n) {
    System.out.print("System.out.println(");
    n.e.accept(this);
    System.out.print(");");
  }

  //Identifier i;
  // Exp e;
  public void visit(AssignExp n) {
    n.i.accept(this);
    System.out.print(" <- ");
    n.e.accept(this);
  }
  
  // Exp e1,e2;
  public void visit(And n) {
    System.out.print("(");
    n.e1.accept(this);
    System.out.print(" && ");
    n.e2.accept(this);
    System.out.print(")");
  }

  // Exp e1,e2;
  public void visit(LessThan n) {
    System.out.print("(");
    n.e1.accept(this);
    System.out.print(" < ");
    n.e2.accept(this);
    System.out.print(")");
  }

  //Exp e1,e2;
  public void visit(LessThanOrEqual n) {
   System.out.print("(");
   n.e1.accept(this);
   System.out.print(" <= ");
   n.e2.accept(this);
   System.out.print(")");
  }

 //Exp e1,e2;
  public void visit(Equal n) {
   System.out.print("(");
   n.e1.accept(this);
   System.out.print(" = ");
   n.e2.accept(this);
   System.out.print(")");
  }
 
  // Exp e;
  public void visit(Not n) {
    System.out.print("!");
    n.e.accept(this);
  }

  // Exp e;
  public void visit(True n) {
    System.out.print("true");
  }

  // Exp e;
  public void visit(Tilde n) {
    System.out.print(" tilde ");
    n.e1.accept(this);
  }
  
  // Exp e;
  public void visit(False n) {
    System.out.print("false");
  }
  
  //Exp e;
  public void visit(IsvoidExp n) {
   System.out.print("isvoid ");
   n.e.accept(this);
  }
  
  // Exp e1,e2;
  public void visit(Plus n) {
    System.out.print("(");
    n.e1.accept(this);
    System.out.print(" + ");
    n.e2.accept(this);
    System.out.print(")");
  }

  // Exp e1,e2;
  public void visit(Minus n) {
    System.out.print("(");
    n.e1.accept(this);
    System.out.print(" - ");
    n.e2.accept(this);
    System.out.print(")");
  }

//Exp e1,e2;
 public void visit(Multiply n) {
   System.out.print("(");
   n.e1.accept(this);
   System.out.print(" * ");
   n.e2.accept(this);
   System.out.print(")");
 }

//Exp e1,e2;
public void visit(Divide n) {
  System.out.print("(");
  n.e1.accept(this);
  System.out.print(" / ");
  n.e2.accept(this);
  System.out.print(")");
}


  // int i;
  public void visit(IntegerLiteral n) {
    System.out.print(n.i);
  }

  public void visit(StringLiteral n) {
	System.out.print(n.s);
  }

  // String s;
  public void visit(IdentifierExp n) {
    System.out.print(n.s);
  }

  public void visit(This n) {
    System.out.print("this");
  }

  // String s;
  public void visit(Identifier n) {
    System.out.print(n.s);
  }
}
