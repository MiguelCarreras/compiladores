package AST.Visitor;

import AST.*;

import Semantic.Symbols.ClassSymbol;
import Semantic.Symbols.MethodSymbol;
import Semantic.Symbols.ParameterSymbol;
import Semantic.Symbols.Symbol;
import Semantic.Symbols.VariableSymbol;
import Semantic.TableSymbol.ClassTable;
import Semantic.TableSymbol.MethodTable;
import Semantic.TableSymbol.TableSymbol;
import Semantic.TypeChecking.TypeCheck;


public class TableSymbolVisitor implements Visitor {

	private TableSymbol tableSymbol;
	private ClassTable classTable;
	private ClassSymbol classSymbol;
	private MethodSymbol methodSymbol;
	private MethodTable methodTable;
	
	public TableSymbolVisitor() {
		this.tableSymbol = new TableSymbol();
		this.classSymbol = null;
		this.classTable = null;
		this.methodSymbol = null;
		this.methodTable = null;
	}
	
	public TableSymbol getTableSymbol() {
		return this.tableSymbol;
	}
	
	public void visit(Display n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(Program n) {
		// TODO Auto-generated method stub
		for ( int i = 0; i < n.cl.size(); i++ ) {
			n.cl.get(i).accept(this);
	    }
	}

	public void visit(MainClass n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(ClassDeclSimple n) {
		// TODO Auto-generated method stub
		IdentifierType id = n.i;
		
		//se actualiza la tabla de simbolos del TypeCheck
		this.classSymbol = new ClassSymbol(n.i.s, n.i.getLine());
		this.classTable = new ClassTable(this.classSymbol);
		this.tableSymbol.addClassTable(this.classTable);
		
		n.i.accept(this);
		for( int i = 0; i < n.fl.size(); i++) {
			n.fl.get(i).accept(this);
		}
		this.classSymbol = null;
		//this.classTable = null;
	}

	public void visit(ClassDeclInherits n) {
		// TODO Auto-generated method stub
		IdentifierType id = n.i;
		//Simbolo para clase padre
		ClassSymbol parentClass = new ClassSymbol(n.j.s, n.j.getLine());
		
		//Agrega la clase a la tabla de simbolos
		this.classSymbol = new ClassSymbol(id.s, parentClass, n.getLine());
		this.classTable = new ClassTable(this.classSymbol);
		this.tableSymbol.addClassTable(this.classTable);
		
		n.i.accept(this);
		n.j.accept(this);
		for (int i = 0; i < n.fl.size(); i++) {
			n.fl.get(i).accept(this);
		}
		this.classSymbol = null;
		this.classTable = null;
	}

	public void visit(VarDecl n) {
		// TODO Auto-generated method stub
		
		//La clase de la variable
		ClassSymbol variableClass = new ClassSymbol(n.t.s, n.t.getLine());
		//La variable puede ser variable de la clase o variable del metodo
		Symbol variableParent = null;
		if(this.methodSymbol == null) {
			variableParent = this.methodSymbol;
		}else {
			variableParent = this.classSymbol;
		}
		
		VariableSymbol variable = new VariableSymbol(n.i.s, variableClass, variableParent, n.i.getLine());
		this.tableSymbol.addVariableToClassTable(this.classTable, variable);
		
		n.t.accept(this);
		n.i.accept(this);
		if(n.e != null) {
	    	n.e.accept(this);
	    }
	}

	public void visit(MethodDecl n) {
		
		//Aun no considera metodos con retorno de valor
		//Se agrega el metodo a la clase actual
		ClassSymbol classReturnet = null;
		
			this.methodSymbol = new MethodSymbol(n.i.s, this.classSymbol, n.i.getLine());
		
		this.methodTable = new MethodTable(this.methodSymbol);
		this.tableSymbol.addMethodTableToClassTable(this.classTable, this.methodTable);
		
		n.i.accept(this);
		for ( int i = 0; i < n.fl.size(); i++ ) {
			 n.fl.get(i).accept(this);
		 }
		n.t.accept(this);
		n.e.accept(this);
		this.methodSymbol = null;
		//this.methodTable = null;
	}

	public void visit(Formal n) {
		// TODO Auto-generated method stub
		//Clase del parametro. Ejemplo: Int, Bool, etc.
		ClassSymbol classParameter = new ClassSymbol(n.t.s, n.t.getLine());
		ParameterSymbol parameter = new ParameterSymbol(n.i.s, classParameter, this.methodSymbol, n.i.getLine());
		this.tableSymbol.addParameterToMethodTable(this.classTable, this.methodTable, parameter);
		
		n.t.accept(this);
	    n.i.accept(this);
		
	}

	public void visit(IntegerType n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(IdentifierType n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(Print n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(AssignExp n) {
		// TODO Auto-generated method stub
	}

	public void visit(DispatchShortExp n) {
		// TODO Auto-generated method stub
	}

	public void visit(DispatchCommonExp n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(DispatchLongExp n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(WhileExp n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(IfExp n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(BlockExp n) {
		// TODO Auto-generated method stub
		for ( int i = 0; i < n.bl.size(); i++ ) {
			n.bl.get(i).accept(this);
		}	   
	}

	public void visit(CaseOptExp n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(CaseExp n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(LetSimpleOptExp n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(LetLongOptExp n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(LetExp n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(NewExp n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(And n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(LessThan n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(IsvoidExp n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(LessThanOrEqual n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(Equal n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(Not n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(True n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(False n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(Tilde n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(Plus n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(Minus n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(Multiply n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(Divide n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(IntegerLiteral n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(StringLiteral n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(IdentifierExp n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(This n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(Identifier n) {
		// TODO Auto-generated method stub
		
	}

}
