package AST.Visitor;

import AST.And;
import AST.AssignExp;
import AST.BlockExp;
import AST.CaseExp;
import AST.CaseOptExp;
import AST.ClassDeclInherits;
import AST.ClassDeclSimple;
import AST.DispatchCommonExp;
import AST.DispatchLongExp;
import AST.DispatchShortExp;
import AST.Display;
import AST.Divide;
import AST.Equal;
import AST.False;
import AST.Formal;
import AST.Identifier;
import AST.IdentifierExp;
import AST.IdentifierType;
import AST.IfExp;
import AST.IntegerLiteral;
import AST.IntegerType;
import AST.IsvoidExp;
import AST.LessThan;
import AST.LessThanOrEqual;
import AST.LetExp;
import AST.LetLongOptExp;
import AST.LetSimpleOptExp;
import AST.MainClass;
import AST.MethodDecl;
import AST.Minus;
import AST.Multiply;
import AST.NewExp;
import AST.Not;
import AST.Plus;
import AST.Print;
import AST.Program;
import AST.StringLiteral;
import AST.This;
import AST.Tilde;
import AST.True;
import AST.VarDecl;
import AST.WhileExp;
import Semantic.Symbols.ClassSymbol;
import Semantic.Symbols.MethodSymbol;
import Semantic.Symbols.ParameterSymbol;
import Semantic.Symbols.Symbol;
import Semantic.Symbols.VariableSymbol;
import Semantic.TableSymbol.ClassTable;
import Semantic.TableSymbol.MethodTable;
import Semantic.TableSymbol.TableSymbol;
import Semantic.TypeChecking.TypeCheck;

public class TypeCheckingVisitor implements Visitor {

	
	private TableSymbol tableSymbol;
	private ClassSymbol classSymbol;
	private ClassTable classTable;
	private MethodSymbol methodSymbol;
	private MethodTable methodTable;
	private TypeCheck chek;
	private Symbol returnedType;
	private Symbol currentScope;
	
	public TypeCheckingVisitor(TableSymbol tableSymbol) {
		this.tableSymbol = tableSymbol;
		this.chek = new TypeCheck(tableSymbol);
		this.classSymbol = null;
		this.methodSymbol = null;
		this.returnedType = null;
		this.currentScope = null;
		
	}
	
	public TableSymbol getTableSymbol() {
		return this.tableSymbol;
	}
	
	public int errorsSize() {
		return this.chek.getErrors().size();
	}
	
	public void printErrors() {
		if(this.chek.getErrors().size() == 0) { 
			System.out.println("Error not found.");
		}else {
			this.chek.printErrors();
		}
		
	}
	
	public void visit(Display n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(Program n) {
		// TODO Auto-generated method stub
		for ( int i = 0; i < n.cl.size(); i++ ) {
			n.cl.get(i).accept(this);
	    }
	}

	public void visit(MainClass n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(ClassDeclSimple n) {
		// TODO Auto-generated method stub
		this.classSymbol = new ClassSymbol(n.i.s, n.i.getLine());
		this.classTable = new ClassTable(this.classSymbol);
		//se verifica si la clase esta duplicada
		this.chek.checkClassDuplicate(classSymbol, classSymbol.getLine());
		
		//se actualiza la tabla de simbolos del TypeCheck
		String key = this.classSymbol.getType()+","+this.classSymbol.getId();
		this.tableSymbol.addSymbol(key, this.classSymbol);
		this.chek.setTableSymbol(this.tableSymbol);
		
		n.i.accept(this);
		for( int i = 0; i < n.fl.size(); i++) {
			n.fl.get(i).accept(this);
		}
		
		this.classSymbol = null;
	}

	public void visit(ClassDeclInherits n) {
		// TODO Auto-generated method stub
		//se verifica que la clase de la cual hereda exista
		ClassSymbol inheritClass = new ClassSymbol(n.j.s, n.j.getLine());
		this.chek.checkClassExist(inheritClass, n.j.getLine());
		
		this.classSymbol = new ClassSymbol(n.i.s, n.i.getLine());
		this.classTable = new ClassTable(this.classSymbol);
		//se verifica si la clase esta duplicada
		this.chek.checkClassDuplicate(classSymbol, classSymbol.getLine());
		
		//se actualiza la tabla de simbolos del TypeCheck
		String key = this.classSymbol.getType()+","+this.classSymbol.getId();
		this.tableSymbol.addSymbol(key, this.classSymbol);
		//key = inheritClass.getType()+","+inheritClass.getId();
		//this.tableSymbol.addSymbol(key, inheritClass);
		this.chek.setTableSymbol(this.tableSymbol);
		
		n.i.accept(this);
		n.j.accept(this);
		for( int i = 0; i < n.fl.size(); i++) {
			n.fl.get(i).accept(this);
		}
		
		this.classSymbol = null;
	}

	public void visit(VarDecl n) {
		// TODO Auto-generated method stub
		
		ClassSymbol variableClass = new ClassSymbol(n.t.s, n.t.getLine());
		//La variable puede ser variable de la clase o variable del metodo
		
		ClassSymbol	variableParent = this.classSymbol;
		
		VariableSymbol variable = new VariableSymbol(n.i.s, variableClass, variableParent, n.i.getLine());
		this.tableSymbol.addVariableToClassTable(this.classTable, variable);
		this.chek.checkVariableDuplicateInClass(variable, variableParent, variable.getLine());
		
		String key = variable.getType()+","+variable.getId()+","+variableParent.getId();
		this.tableSymbol.addSymbol(key, variable);
		this.chek.setTableSymbol(this.tableSymbol);
		
		n.t.accept(this);
		n.i.accept(this);
		if(n.e != null) {
	    	n.e.accept(this);
	    	this.chek.checkCorrectType(variable, this.returnedType.getId(), n.e.getLine());
		}
	}

	public void visit(MethodDecl n) {
		//se verifica que el metodo no sea duplicado
		this.methodSymbol = new MethodSymbol(n.i.s, this.classSymbol, n.i.getLine());
		this.methodTable = new MethodTable(this.methodSymbol);
		this.tableSymbol.addMethodTableToClassTable(this.classTable, this.methodTable);
		this.chek.checkMethodDuplicate(this.methodSymbol, this.classSymbol, n.i.getLine());
		//se verifica que la clase retornada exista
		if(n.t != null) {
			if(!n.t.s.equals("SELF_TYPE")) {
				ClassSymbol classReturned = new ClassSymbol(n.t.s, this.classSymbol, n.t.getLine());
				this.methodSymbol.setClassReturned(classReturned);
				this.chek.checkClassExist(classReturned, n.t.getLine());
			} else {
				ClassSymbol classReturned = new ClassSymbol(this.classSymbol.getId(), this.classSymbol, n.t.getLine());
				this.methodSymbol.setClassReturned(classReturned);
			}
		}
		
		
		String key = this.methodSymbol.getType()+","+this.methodSymbol.getId()+","+this.methodSymbol.getParent().getId();
		this.tableSymbol.addSymbol(key, this.methodSymbol);
		this.chek.setTableSymbol(this.tableSymbol);

		n.i.accept(this);
		for ( int i = 0; i < n.fl.size(); i++ ) {
			 n.fl.get(i).accept(this);
		 }
		n.t.accept(this);
		n.e.accept(this);
		this.methodSymbol = null;
	}

	public void visit(Formal n) {
		// TODO Auto-generated method stub	
		ClassSymbol formalClassSymbol = new ClassSymbol(n.t.s, n.t.getLine());
		this.chek.checkClassExist(formalClassSymbol, formalClassSymbol.getLine());
		
		ParameterSymbol parameter = new ParameterSymbol(n.i.s, formalClassSymbol, this.methodSymbol, n.i.getLine());
		this.tableSymbol.addParameterToMethodTable(this.classTable, this.methodTable, parameter);
		String key = parameter.getType()+","+parameter.getId()+","+parameter.getParent().getId();
		this.tableSymbol.addSymbol(key, parameter);
		
		this.chek.setTableSymbol(this.tableSymbol);
		
		n.t.accept(this);
	    n.i.accept(this);
		
	}

	public void visit(IntegerType n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(IdentifierType n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(Print n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(AssignExp n) {
		// TODO Auto-generated method stub
		n.i.accept(this);
	    n.e.accept(this);
	}

	public void visit(DispatchShortExp n) {
		// TODO Auto-generated method stub
		MethodSymbol methodSymbol = new MethodSymbol(n.i.s, this.classSymbol, n.i.getLine());
		this.chek.checkMethodExist(methodSymbol, n.i.getLine());
		this.chek.checkParameterQtyForMehtod(methodSymbol, n.el.size(), n.el.getLine());
		n.i.accept(this);   
		for ( int i = 0; i < n.el.size(); i++ ) {
			n.el.get(i).accept(this);
		}
	}

	public void visit(DispatchCommonExp n) {
		// TODO Auto-generated method stub
		n.e.accept(this);
		n.i.accept(this);
		for ( int i = 0; i < n.el.size(); i++ ) {
			n.el.get(i).accept(this);
		}
		 
	}

	public void visit(DispatchLongExp n) {
		// TODO Auto-generated method stub
		n.e.accept(this);
	    if(n.it != null) {
			 n.it.accept(this); 
		}
		 n.i.accept(this);
		 for ( int i = 0; i < n.el.size(); i++ ) {
		     n.el.get(i).accept(this);
		 }
	}

	public void visit(WhileExp n) {
		// TODO Auto-generated method stub
		n.e1.accept(this);
		n.e2.accept(this);
		
	}

	public void visit(IfExp n) {
		// TODO Auto-generated method stub
		n.e1.accept(this);
		n.e2.accept(this);
		n.e3.accept(this);
		  
	}

	public void visit(BlockExp n) {
		// TODO Auto-generated method stub
		for ( int i = 0; i < n.bl.size(); i++ ) {
			n.bl.get(i).accept(this);
		}	   
	}

	public void visit(CaseOptExp n) {
		// TODO Auto-generated method stub
		n.i.accept(this);
		n.it.accept(this);
		n.e.accept(this);
	}

	public void visit(CaseExp n) {
		// TODO Auto-generated method stub
		  n.e.accept(this);
		  for ( int i = 0; i < n.col.size(); i++ ) {
			  n.col.get(i).accept(this);
		  }
	}

	public void visit(LetSimpleOptExp n) {
		// TODO Auto-generated method stub
		ClassSymbol classSymbol = new ClassSymbol(n.it.s, n.it.getLine());
		this.chek.checkClassExist(classSymbol, n.it.getLine());
		n.i.accept(this);
		n.it.accept(this);
	}

	public void visit(LetLongOptExp n) {
		// TODO Auto-generated method stub
		ClassSymbol classSymbol = new ClassSymbol(n.it.s, n.it.getLine());
		this.chek.checkClassExist(classSymbol, n.it.getLine());
		
		n.i.accept(this);
		n.it.accept(this);
		n.e.accept(this);
	}

	public void visit(LetExp n) {
		// TODO Auto-generated method stub
		 for ( int i = 0; i < n.lol.size(); i++ ) {
			  n.lol.get(i).accept(this);
		 }
		 n.e.accept(this);
	}

	public void visit(NewExp n) {
		// TODO Auto-generated method stub
		ClassSymbol classReturned = new ClassSymbol(n.it.s, n.it.getLine());
		this.chek.checkClassExist(classReturned, n.it.getLine());
		this.returnedType = classReturned;
		
		n.it.accept(this);
		
	}
	
	public void visit(And n) {
		// TODO Auto-generated method stub
		this.returnedType = this.tableSymbol.getClassTable("Bool").getClassSymbol();
		
		n.e1.accept(this);
		n.e2.accept(this);
	}

	public void visit(LessThan n) {
		// TODO Auto-generated method stub
		this.returnedType = this.tableSymbol.getClassTable("Bool").getClassSymbol();
		
	    n.e1.accept(this);
	    n.e2.accept(this);
	}

	public void visit(LessThanOrEqual n) {
		// TODO Auto-generated method stub
		this.returnedType = this.tableSymbol.getClassTable("Bool").getClassSymbol();
		
		n.e1.accept(this);
		n.e2.accept(this);
	}

	public void visit(Equal n) {
		// TODO Auto-generated method stub
		this.returnedType = this.tableSymbol.getClassTable("Bool").getClassSymbol();
		
		n.e1.accept(this);
		n.e2.accept(this);
	}

	public void visit(Not n) {
		// TODO Auto-generated method stub
		this.returnedType = this.tableSymbol.getClassTable("Bool").getClassSymbol();
		
		n.e.accept(this);
	}

	public void visit(True n) {
		// TODO Auto-generated method stub
		this.returnedType = this.tableSymbol.getClassTable("Bool").getClassSymbol();
		
	}

	public void visit(False n) {
		// TODO Auto-generated method stub
		this.returnedType = this.tableSymbol.getClassTable("Bool").getClassSymbol();
		
	}

	public void visit(Tilde n) {
		// TODO Auto-generated method stub
		this.returnedType = this.tableSymbol.getClassTable("Bool").getClassSymbol();
		n.e1.accept(this);
	}

	public void visit(IsvoidExp n) {
		// TODO Auto-generated method stub
		this.returnedType = this.tableSymbol.getClassTable("Bool").getClassSymbol();
		n.e.accept(this);
	}
	
	public void visit(Plus n) {
		// TODO Auto-generated method stub
		n.e1.accept(this);
		n.e2.accept(this);
	}

	public void visit(Minus n) {
		// TODO Auto-generated method stub
		n.e1.accept(this);
	    n.e2.accept(this);
	}

	public void visit(Multiply n) {
		// TODO Auto-generated method stub
		n.e1.accept(this);
		n.e2.accept(this);
	}

	public void visit(Divide n) {
		// TODO Auto-generated method stub
		n.e1.accept(this);
		n.e2.accept(this);
	}

	public void visit(IntegerLiteral n) {
		// TODO Auto-generated method stub
		this.returnedType = this.tableSymbol.getClassTable("Int").getClassSymbol();
	}

	public void visit(StringLiteral n) {
		// TODO Auto-generated method stub
		this.returnedType = this.tableSymbol.getClassTable("String").getClassSymbol();
	}

	public void visit(IdentifierExp n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(This n) {
		// TODO Auto-generated method stub
		
	}

	public void visit(Identifier n) {
		// TODO Auto-generated method stub
		
	}

}
