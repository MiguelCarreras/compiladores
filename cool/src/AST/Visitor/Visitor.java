package AST.Visitor;

import AST.*;

public interface Visitor {
  public void visit(Display n);
  public void visit(Program n);
  public void visit(MainClass n);
  public void visit(ClassDeclSimple n);
  public void visit(ClassDeclInherits n);
  public void visit(VarDecl n);
  public void visit(MethodDecl n);
  public void visit(Formal n);
  public void visit(IntegerType n);
  public void visit(IdentifierType n);
  public void visit(Print n);
  public void visit(AssignExp n);
  public void visit(DispatchShortExp n);
  public void visit(DispatchCommonExp n);
  public void visit(DispatchLongExp n);
  public void visit(WhileExp n);
  public void visit(IfExp n);
  public void visit(BlockExp n);
  public void visit(CaseOptExp n);
  public void visit(CaseExp n);
  public void visit(LetSimpleOptExp n);
  public void visit(LetLongOptExp n);
  public void visit(LetExp n);
  public void visit(NewExp n);
  public void visit(And n);
  public void visit(LessThan n);
  public void visit(IsvoidExp n);
  public void visit(LessThanOrEqual n);
  public void visit(Equal n);
  public void visit(Not n);
  public void visit(True n);
  public void visit(False n);
  public void visit(Tilde n);
  public void visit(Plus n);
  public void visit(Minus n);
  public void visit(Multiply n);
  public void visit(Divide n);
  public void visit(IntegerLiteral n);
  public void visit(StringLiteral n);
  public void visit(IdentifierExp n);
  public void visit(This n);
  public void visit(Identifier n);
}
