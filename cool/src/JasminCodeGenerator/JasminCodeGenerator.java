package JasminCodeGenerator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import AST.Exp;
import AST.Formal;
import AST.FormalList;
import AST.Type;
import Semantic.Symbols.ParameterSymbol;
import Semantic.TableSymbol.MethodTable;

public class JasminCodeGenerator {
	
	private Stack<Integer> stacker;
	private static List<String> listCode;
	private static String fileDir;
	private int contLabel = 0;
	
	public JasminCodeGenerator(String fileDir) {
		setFileDir(fileDir);
		stacker = new Stack<Integer>();
	}

	public static String getFileDir() {
		return fileDir;
	}

	public void setFileDir(String fileDir) {
		JasminCodeGenerator.fileDir = fileDir;
	}

	public static List<String> getListCode() {
		return listCode;
	}

	public static void setListCode(List<String> listCode) {
		JasminCodeGenerator.listCode = listCode;
	}
	
	public void bList() {
		setListCode(new ArrayList<String>());
	}
	
	public void addListCode(String code) {
		listCode.add(code);
	}
	
	public String getLastListCode() {
		return listCode != null && !listCode.isEmpty() ? listCode.get(listCode.size() - 1) : null;
	}
	
	public void addListCodeToLast(String code) {
		listCode.remove(listCode.size()-1);
		listCode.add(listCode.size(), code);
	}
	
	public void defaultConstructor(List<String> newObjectVar, String classInherit) {
		addListCode("\n.method public <init>()V");
		addListCode("\t.limit stack 100");
		addListCode("\t.limit locals 100");
		addListCode("\taload_0");
		if (classInherit == null) classInherit = "java/lang/Object"; 
		addListCode("\tinvokespecial "+classInherit+"/<init>()V");
		if(newObjectVar != null && !newObjectVar.isEmpty()) {
			for(int i = 0;  i <= newObjectVar.size()-1; i++) {
				if(i % 4 == 0) {
					addListCode("\taload_0");
				}
				addListCode(newObjectVar.get(i));
			}
		}
		addListCode("\treturn");
		addListCode(".end method \n");
	}
	
	public void classSchedule(String className) {
		classInheritSchedule(className, "java/lang/Object \n");
	}
	
	public void classInheritSchedule(String className, String inheritClass) {
		bList();
		addListCode(".source "+className+".j");
		addListCode(".class  public "+className);
		addListCode(".super  "+inheritClass);
	}
	
	public void varDeclarationSchedule(String varName, String varClassName) {
		addListCode(".field public "+varName+" "+varClassName);
	}
	
	public void methodInitSchedule(MethodTable method, FormalList fl) {
		resetStack();
		String methodSyntax = ".method public "+method.getMethodName()+"(";
		for(int i=0; i<fl.size(); i++) {
			methodSyntax += defineJasminClass(fl.get(i).t.s);
		}
		methodSyntax += ")"+defineJasminClass(method.getReturnedMethodClassName());
		addListCode("\n"+methodSyntax);
		addListCode("\t.limit stack 100");
		addListCode("\t.limit locals 100");
	}
	
	public void methodEndSchedule(String classReturned) {
		addListCode("\t"+defineJasminReturn(classReturned));
		addListCode(".end method");
	}
	
	public void methodMainInitSchedule() {
		resetStack();
		addListCode("\n.method public static main([Ljava/lang/String;)V");
		addListCode("\t.limit stack 100");
		addListCode("\t.limit locals 100");
	}
	
	public void methodMainEndSchedule() {
		addListCode("\treturn");
		addListCode(".end method");
	}
	
	public String defineComand(String comand, int numberComand) {
		if(comand.equals("iconst")) {
			if(numberComand == -1 ) {
				return "iconst_m1";
			}else if(numberComand >= 0 && numberComand <= 5) {
				return "iconst_"+numberComand;
			} else {
				return "ldc "+numberComand;
			}
		} else {
			return defaultComand(comand, numberComand);
		}
	}
	
	public String defaultComand(String comand, int numberComand) {
		if(numberComand >= 0 && numberComand <= 3) {
			return comand+"_"+numberComand;
		} else {
			return comand+" "+numberComand;
		}
	}
	
	public void lessThanLabel(Exp e) {
		String trueL = jasminLabel("true");
		String falseL = jasminLabel("false");
		addListCode("\tif_icmplt "+trueL);
		//decrementStack(2);
		addListCode("\t"+defineComand("iconst", 1));
		addListCode("\tgoto "+falseL+"\n");
		newLabel(trueL);
		addListCode("\t"+defineComand("iconst", 0));
		incrementStack();
		newLabel(falseL);
		addListCode("\n");
	}
	
	public void lessThanOrEqLabel(Exp e) {
		String trueL = jasminLabel("true");
		String falseL = jasminLabel("false");
		addListCode("\tif_icmple "+trueL);
		//decrementStack(2);
		addListCode("\t"+defineComand("iconst", 1));
		addListCode("\tgoto "+falseL+"\n");
		newLabel(trueL);
		addListCode("\t"+defineComand("iconst", 0));
		incrementStack();
		newLabel(falseL);
		addListCode("\n");
	}
	
	public void equalLabel(Exp e) {
		String trueL = jasminLabel("true");
		String falseL = jasminLabel("false");
		addListCode("\tif_icmpeq "+trueL);
		//decrementStack(2);
		addListCode("\t"+defineComand("iconst", 1));
		addListCode("\tgoto "+falseL+"\n");
		newLabel(trueL);
		addListCode("\t"+defineComand("iconst", 0));
		incrementStack();
		newLabel(falseL);
		addListCode("\n");
	}
	
	public void newLabel(String nameLabel) {
		addListCode("\t"+nameLabel+":");
	}
	
	public String jasminLabel(String nameLabel) {
		contLabel++;
		return nameLabel+"-"+contLabel;
	}
	
	public void writeInFile(FileWriter jasminFile) throws IOException{
		for(String l : listCode) {
			jasminFile.append(l);
			jasminFile.append("\n");
		}
	}
	
	public void generateJasminFile(String fileName) {
		String file = fileDir+File.separator+fileName+".j";
		try {
		    // Create file .j
			FileWriter jasminFile = new FileWriter(file);
			writeInFile(jasminFile);
			jasminFile.close();
		}catch(IOException e) {
			System.out.println("Error: No se pudo escribir en el archivo");
		}
	}
	
	public void clearDirectory() {
		File directory = new File(fileDir);
		File f;
		if (directory.isDirectory()) {
			String[] files = directory.list();
			for (String fileAux : files) {
				f = new File(fileDir + File.separator + fileAux);
				f.delete();
			}
		}
	}
	
	public void incrementStack() {
		stacker.push(1);
		stacker.push(1);
	}
	
	public void decrementStack(int numberOfDecrement) {
		for(int i = numberOfDecrement; i > 0; i--) {
			stacker.pop();
		}
	}
	
	public void resetStack() {
		stacker.clear();
	}
	
	public String defineJasminClass(String varClass) {
		if(varClass.equals("Int")) {
			return "I";
		}else if(varClass.equals("String")) {
			return "Ljava/lang/String;";
		}else if(varClass.equals("Bool")) {
			return "I";
		}else if(varClass.equals("Object")) {
			return "Ljava/lang/Object;";
		}else {
			return "L"+varClass+";";
		}
	}
	
	public String defineJasminClass2(String varClass) {
		if(varClass.equals("Int")) {
			return "I";
		}else if(varClass.equals("String")) {
			return "Ljava/lang/String";
		}else if(varClass.equals("Bool")) {
			return "B";
		}else if(varClass.equals("Object")) {
			return "Ljava/lang/Object";
		}else {
			return "L"+varClass;
		}
	}
	
	public String defineJasminReturn(String classType) {
		if(classType.equals("Int") || classType.equals("Bool")) {
			return "ireturn";
		}else {
			return "areturn";
		}
	}
	
	public String defineJasminLoad(String classType) {
		if(classType.equals("Int") || classType.equals("Bool")) {
			return "iload";
		}else {
			return "aload";
		}
	}
}
