/*
 * JFlex specification for the lexical analyzer for a simple demo language.
 * Change this into the scanner for your implementation of Cool.
 */


package Scanner;

import java_cup.runtime.*;
import Parser.sym;

%%

%public
%final
%class scanner
%unicode
%cup
%line
%column

/* Code copied into the generated scanner class.  */
/* Can be referenced in scanner action code. */
%{
  // Return new symbol objects with line and column numbers in the symbol 
  // left and right fields. This abuses the original idea of having left 
  // and right be character positions, but is   // is more useful and 
  // follows an example in the JFlex documentation.
  private Symbol symbol(int type) {
    return new Symbol(type, yyline+1, yycolumn+1);
  }
  private Symbol symbol(int type, Object value) {
    return new Symbol(type, yyline+1, yycolumn+1, value);
  }

  // Return a readable representation of symbol s (aka token)
  public String symbolToString(Symbol s) {
    String rep;
    switch (s.sym) {
      /* reserved words */
      case sym.DISPLAY: return "DISPLAY";
      case sym.CLASS: return "CLASS";
      case sym.INHERITS: return "INHERITS";
      case sym.IF: return "IF";
      case sym.THEN: return "THEN";
      case sym.FI: return "FI";
      case sym.WHILE: return "WHILE";
      case sym.LOOP: return "LOOP";
      case sym.POOL: return "POOL";
      case sym.LET: return "LET";
      case sym.IN: return "IN";
      case sym.CASE: return "CASE";
      case sym.OF: return "OF";
      case sym.ESAC: return "ESAC";
      case sym.NEW: return "NEW";
      case sym.ISVOID: return "ISVOID";
      case sym.FALSE: return "FALSE";
      case sym.TRUE: return "TRUE";
      case sym.NOT: return "NOT";
	  	
	  /* operators */	      
      case sym.PLUS: return "PLUS";
      case sym.MINUS: return "MINUS";
      case sym.MULT: return "MULT";
      case sym.DIV: return "DIV";
      case sym.LESS: return "LESS";
      case sym.LESS_EQL: return "LESS_EQL";
      case sym.EQL: return "EQL"; 
	  case sym.POINT: return "POINT";
	  case sym.AT: return "AT";
	  case sym.TILDE: return "TILDE";
	  case sym.ASSIGN: return "ASSIGN";
	  case sym.CASE_OPERATOR: return "CASE_OPERATOR";

	  /* delimiters */      
      case sym.SEMICOLON: return "SEMICOLON";
      case sym.COLON: return "COLON";
      case sym.COMMA: return "COMMA";
      case sym.LPAREN: return "LPAREN";
      case sym.RPAREN: return "RPAREN";
      case sym.LBRACE: return "LBRACE";
      case sym.RBRACE: return "RBRACE";
      case sym.LBRACKET: return "LBRACKET";
      case sym.RBRACKET: return "RBRACKET";
      
      case sym.TYPE: return "TYPE(" + (String)s.value + ")";
      case sym.IDENTIFIER: return "ID(" + (String)s.value + ")";
      case sym.INTEGER_LITERAL: return "INTEGER_LITERAL(" + (String)s.value + ")";
      case sym.STRING: return "STRING(" + (String)s.value + ")";
      case sym.EOF: return "<EOF>";
      case sym.error: return "<ERROR>";
      default: return "<UNEXPECTED TOKEN " + s.toString() + ">";
    }
  }
%}

/* Helper definitions */
letter = [a-zA-Z]
capitalLetter = [A-Z] 
digit = [0-9]
string = \"([^\\\"]|\\.)*\"
eol = [ \r\n\t]
white = {eol}


lineComment = "--".*

blockComment = "(*" [^*] ~"*)" | "(*" "*"+ ")"

comment = {blockComment} | {lineComment}

%%

/* Token definitions */

/* reserved words */

/* (put here so that reserved words take precedence over identifiers) */
(C|c)(L|l)(A|a)(S|s)(S|s) { return symbol(sym.CLASS); }
"display" { return symbol(sym.DISPLAY); }
"class" { return symbol(sym.CLASS); }
"inherits" { return symbol(sym.INHERITS); }
"if" { return symbol(sym.IF); }
"then" { return symbol(sym.THEN); }
"else" { return symbol(sym.ELSE); }
"fi" { return symbol(sym.FI); }
"while" { return symbol(sym.WHILE); }
"loop" { return symbol(sym.LOOP); }
"pool" { return symbol(sym.POOL); }
"let" { return symbol(sym.LET); }
"in" { return symbol(sym.IN); }
"case" { return symbol(sym.CASE); }
"of" { return symbol(sym.OF); }
"esac" { return symbol(sym.ESAC); }
"new" { return symbol(sym.NEW); }
"isvoid" { return symbol(sym.ISVOID); }
"true" { return symbol(sym.TRUE); }
"false" { return symbol(sym.FALSE); }
"not" { return symbol(sym.NOT); }


/* operators */
"+" { return symbol(sym.PLUS); }
"-" { return symbol(sym.MINUS); }
"/" { return symbol(sym.DIV); }
"*" { return symbol(sym.MULT); }
"=" { return symbol(sym.EQL); }
"<" { return symbol(sym.LESS); }
"<=" { return symbol(sym.LESS_EQL); }
"." { return symbol(sym.POINT); }
"@" { return symbol(sym.AT); }
"~" { return symbol(sym.TILDE); }
"<-" { return symbol(sym.ASSIGN); }
"=>" { return symbol(sym.CASE_OPERATOR); }

/* delimiters */
"(" { return symbol(sym.LPAREN); }
")" { return symbol(sym.RPAREN); }
";" { return symbol(sym.SEMICOLON); }
":" { return symbol(sym.COLON); }
"," { return symbol(sym.COMMA); }
"{" { return symbol(sym.LBRACE); }
"}" { return symbol(sym.RBRACE); }
"[" { return symbol(sym.LBRACKET); }
"]" { return symbol(sym.RBRACKET); }

/* types */
({capitalLetter} ({letter}|{digit}|_)*) { return symbol(sym.TYPE, yytext()); }

/* identifiers */
({letter}) ({letter}|{digit}|_)* { return symbol(sym.IDENTIFIER, yytext()); }

/* number */
{digit}{digit}* { return symbol(sym.INTEGER_LITERAL, yytext()); }

/* string */

{string}+ { return symbol(sym.STRING, yytext()); }

/* whitespace */
{white}+ { /* ignore whitespace */ }

/* comments*/
{comment}+ { /* ignore whitespace */ }


/* lexical errors (put last so other matches take precedence) */
. { System.err.println(
	"\nunexpected character in input: '" + yytext() + "' at line " +
	(yyline+1) + " column " + (yycolumn+1));
  }
