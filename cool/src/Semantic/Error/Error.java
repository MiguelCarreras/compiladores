package Semantic.Error;

public class Error {
	private String message;
	private int line;
	
	public Error(String message, int line) {
		this.setMessage(message);
		this.setLine(line);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}
	
	public String getFullMessage(int errorNumber) {
		return "Error#"+errorNumber+": En la linea "+getLine()+", "+getMessage()+".";
	}
}
