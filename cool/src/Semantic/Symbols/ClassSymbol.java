package Semantic.Symbols;

public class ClassSymbol extends Symbol{

	public static final String symbolType = "class";
	
	//para el caso de una clase su padre es la clase de la cual hereda
	public ClassSymbol(String id, ClassSymbol classInherit, int line) {
		super(id, symbolType, classInherit, line);
		// TODO Auto-generated constructor stub
	}

	public ClassSymbol(String id, int line) {
		super(id, symbolType, null, line);
		// TODO Auto-generated constructor stub
	}
	
	public String getClassInheritName() {
		if(this.getParent() == null) {
			return null;
		}else {
			return this.getParent().getId();
		}
	}
	
	public ClassSymbol getClassInherit() {
		return (ClassSymbol) this.getParent();
	}
}
