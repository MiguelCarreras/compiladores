package Semantic.Symbols;

public class MethodSymbol extends Symbol{

	public static final String symbolType = "method";
	
	private ClassSymbol classReturned;
	
	//para el caso de un metodo su padre solamente puede ser una clase
	public MethodSymbol(String id, ClassSymbol classReturned, Symbol classParent, int line) {
		super(id, symbolType, classParent, line);
		this.setClassReturned(classReturned);
		// TODO Auto-generated constructor stub
	}

	public MethodSymbol(String id, Symbol classParent, int line) {
		super(id, symbolType, classParent, line);
		this.setClassReturned(null);
	}
	public ClassSymbol getClassReturned() {
		return classReturned;
	}

	public void setClassReturned(ClassSymbol classReturned) {
		this.classReturned = classReturned;
	}
	
	public String getClassReturnedName() {
		return this.classReturned.getId();
	}
}
