package Semantic.Symbols;

public class ParameterSymbol extends Symbol{

	public static final String symbolType = "parameter";
	private ClassSymbol classParameter;
	
	//para el caso del parametro el padre es un metodo
	public ParameterSymbol(String id, ClassSymbol  classParameter, Symbol parent, int line) {
		super(id, symbolType, parent, line);
		this.classParameter = classParameter;
		// TODO Auto-generated constructor stub
	}
	
	public String getParameterClassName() {
		return this.classParameter.getId();
	}

}
