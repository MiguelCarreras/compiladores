package Semantic.Symbols;

public abstract class Symbol {
	
	//representa el nombre del simbolo
	private String id;
	//representa el tipo de symbolo
	private String type;
	//representa el padre del simbolo
	private Symbol parent;
	//representa la linea
	private int line;
	
	public Symbol(String id, String type, Symbol parent, int line) {
		this.setId(id);
		this.setType(type);
		this.setParent(parent);
		this.setLine(line);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Symbol getParent() {
		return parent;
	}

	public void setParent(Symbol parent) {
		this.parent = parent;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
