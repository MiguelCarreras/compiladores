package Semantic.Symbols;

public class VariableSymbol extends Symbol{
	
	public static final String symbolType = "variable";
	//Define la clase de la variable, ejemplo: IO, String o Bool
	private ClassSymbol variableClass;
	
	//Para el caso de la variable los posibles padres son una clase o un metodo
	public VariableSymbol(String id, ClassSymbol variableClass, Symbol parent, int line) {
		super(id, symbolType, parent, line);
		this.setVariableClass(variableClass);
		// TODO Auto-generated constructor stub
	}
	
	public void setVariableClass(ClassSymbol variableClass) {
		this.variableClass = variableClass;
		this.setType(variableClass.getId()) ;
	}

	public ClassSymbol getVariableClass() {
		return this.variableClass;
	}
	
	public String getVariableClassName() {
		return this.variableClass.getId();
	}
}
