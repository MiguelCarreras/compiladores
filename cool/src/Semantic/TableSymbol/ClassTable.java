package Semantic.TableSymbol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import AST.Identifier;
import Semantic.Symbols.ClassSymbol;
import Semantic.Symbols.Symbol;
import Semantic.Symbols.VariableSymbol;
import Semantic.Error.Error;

public class ClassTable {
	private ClassSymbol classSymbol;
	
	private Map<String ,MethodTable> methods;
	private Map<String, VariableSymbol> variables;
	private List<Error> errors;
	
	
	public ClassTable(ClassSymbol classSymbol) {
		this.classSymbol = classSymbol;
		this.errors = new ArrayList<Error>();
		this.variables = new HashMap<String, VariableSymbol>();
		this.methods = new HashMap<String, MethodTable>();
	}
	
	public Map<String, VariableSymbol> getVariables(){
		return this.variables;
	}
	
	public void addVariable(VariableSymbol variable) {
		if(this.variables.containsKey(variable.getId())) {
			String errorMessage = "variable "+variable.getId()+"de tipo "+variable.getVariableClassName()+" esta duplicada.";
			this.errors.add(new Error(errorMessage, variable.getLine()));
		} else {
			this.variables.put(variable.getId(), variable);
		}
	}
	
	public VariableSymbol getVariable(String varName) {
		return this.variables.get(varName);
	}
	
	public List<Error> getErrors(){
		return this.errors;
	}
	
	public String getClassName() {
		return this.classSymbol.getId();
	}
	
	public String getClassInheritName() {
		return this.classSymbol.getClassInheritName();
	}
	
	public  Map<String, MethodTable> getMethods(){
    	return this.methods;
    }
	
	public MethodTable getMethodTable(String methodName) {
		return this.methods.get(methodName);
	}
	
	public void addMethod(MethodTable method) {
		this.methods.put(method.getMethodName(), method);
	}
	
	public int getLine() {
		return this.classSymbol.getLine();
	}
	
	public ClassSymbol getClassSymbol() {
		return this.classSymbol;
	}
}
