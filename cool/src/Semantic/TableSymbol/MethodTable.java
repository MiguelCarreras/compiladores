package Semantic.TableSymbol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Semantic.Symbols.MethodSymbol;
import Semantic.Symbols.ParameterSymbol;
import Semantic.Symbols.Symbol;
import Semantic.Symbols.VariableSymbol;
import Semantic.Error.Error;

public class MethodTable {
	private MethodSymbol methodSymbol;
	//se utilizara para comprobar que la cantidad de parametros pasados
	//es igual al requerido
	private int paramsQty = 0;
	//se mantendra una lista de parametros
	private Map<String, ParameterSymbol> parameters;
	//se mantendra el orden de los parametros
	private Map<String, Integer> parametersOrder;
	//se utilizara para comprobar si la variable se encuentra duplicada
	//la clave es el nombre de la variable, por medio de este se verificara
	//si la variable esta duplicada
	private Map<String, VariableSymbol> variables;
	//se mantendra una lista de errores
	private List<Error> errors;
	
	public MethodTable(MethodSymbol methodSymbol) {
		this.methodSymbol = methodSymbol;
		this.errors = new ArrayList<Error>();
		this.parameters = new HashMap<String, ParameterSymbol>();
		this.parametersOrder = new HashMap<String, Integer>();
		this.variables = new HashMap<String, VariableSymbol>();
	}
	
	public String getMethodName() {
		return this.methodSymbol.getId();
	}
	
	public Symbol getMethodParent() {
		return this.methodSymbol.getParent();
	}
	
	public int getParamsQty() {
		return this.paramsQty;
	}
	
	public void setParamsQty(int paramsQty) {
		this.paramsQty = paramsQty;
	}

	public Map<String, ParameterSymbol> getParameters() {
		return parameters;
	}
	
	public ParameterSymbol getParameter(String name) {
		return parameters.get(name);
	}
	
	public int getParameterIndex(String name) {
		return parametersOrder.get(name);
	}
	
	public void addParameter(ParameterSymbol parameter) {
		this.parameters.put(parameter.getId(), parameter);
		this.paramsQty++;
		this.parametersOrder.put(parameter.getId(), this.paramsQty);
	}
	
	public Map<String, VariableSymbol> getVariables(){
		return this.variables;
	}

	public String getReturnedMethodClassName() {
		return this.methodSymbol.getClassReturnedName();
	}
	
	public void addVariable(VariableSymbol variable) {
		//Se comprueba si la variable esta duplicada
		if(this.variables.containsKey(variable.getId())) {
			String errorMessage = "variable "+variable.getId()+"de tipo "+variable.getVariableClassName()+" esta duplicada.";
			this.errors.add(new Error(errorMessage, variable.getLine()));
		}else {
			this.variables.put(variable.getId(), variable);
		}
	}
}
