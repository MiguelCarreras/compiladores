package Semantic.TableSymbol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import Semantic.Error.Error;
import Semantic.Symbols.ParameterSymbol;
import Semantic.Symbols.Symbol;
import Semantic.Symbols.VariableSymbol;

public class TableSymbol {
	private Map<String, ClassTable> classTable;
	private List<Error> errors;
	private Map<String, Symbol> symbols;
	
	public TableSymbol() {
		this.classTable = new HashMap<String, ClassTable>();
		this.errors = new ArrayList<Error>();
		this.symbols = new HashMap<String, Symbol>();
	}
	
	public Map<String, ClassTable> getClassTable(){
		return this.classTable;
	}
	
	public void addSymbol(String symbolKey, Symbol symbol) {
		this.symbols.put(symbolKey, symbol);
	}
	
	public Symbol getSymbol(String key) {
		return this.symbols.get(key);
	}
	
	public void addClassTable(ClassTable classTable) {
		this.classTable.put(classTable.getClassName(), classTable);
	}
	
	public ClassTable getClassTable(String className) {
		return this.classTable.get(className);
	}
	
	public void addMethodTableToClassTable(ClassTable classTable, MethodTable methodTable) {
		this.classTable.get(classTable.getClassName()).addMethod(methodTable);
	}
	
	public MethodTable getMethodTableOfClassTable(String className, String methodName) {
		return getClassTable(className).getMethods().get(methodName);
	}
	
	public void addVariableToClassTable(ClassTable classTable ,VariableSymbol variable) {
		this.classTable.get(classTable.getClassName()).addVariable(variable);
	}
	
	public void addVariableToMethodTable(ClassTable classTable, MethodTable methodTable, VariableSymbol variable) {
		getMethodTableOfClassTable(classTable.getClassName(), methodTable.getMethodName()).addVariable(variable);
	}
	
	public void addParameterToMethodTable(ClassTable classTable, MethodTable methodTable, ParameterSymbol parameter) {
		getMethodTableOfClassTable(classTable.getClassName(), methodTable.getMethodName()).addParameter(parameter);
	}
	
	public VariableSymbol getVariableSymbolToClassTable(String className, String varName) {
		return getClassTable(className).getVariables().get(varName);
	}
}
