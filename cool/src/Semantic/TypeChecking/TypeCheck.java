package Semantic.TypeChecking;

import java.util.ArrayList;
import java.util.List;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.IO;

import Semantic.Error.Error;
import Semantic.Symbols.ClassSymbol;
import Semantic.Symbols.MethodSymbol;
import Semantic.Symbols.ParameterSymbol;
import Semantic.Symbols.VariableSymbol;
import Semantic.TableSymbol.ClassTable;
import Semantic.TableSymbol.MethodTable;
import Semantic.TableSymbol.TableSymbol;

public class TypeCheck {
	private TableSymbol tableSymbol;
	private List<Error> errors;
	private int errorCount = 0;
	
	public TypeCheck(TableSymbol tableSymbol) {
		this.tableSymbol = tableSymbol;
		this.errors = new ArrayList<Error>();
		initClasses();
	}
	
	public void setTableSymbol(TableSymbol tableSymbol) {
		this.tableSymbol = tableSymbol;
	}
	
	public void checkClassExist(ClassSymbol classSymbol, int line) {
		if(this.tableSymbol.getClassTable(classSymbol.getId()) == null) {
			agregarError("la clase "+classSymbol.getId()+ " no fue declarada", line);
		}
	}
	
	public void checkClassDuplicate(ClassSymbol classSymbol, int line) {
		String key = classSymbol.getType()+","+classSymbol.getId();
		if(this.tableSymbol.getSymbol(key) != null) {
			agregarError("la clase "+classSymbol.getId()+" esta duplicada", line);
		}
	}
	
	public void checkVariableDuplicateInClass(VariableSymbol variable, ClassSymbol classParent, int line) {
		String key = variable.getType()+","+variable.getId()+","+classParent.getId();
		
		if(this.tableSymbol.getSymbol(key) != null) {
			agregarError("la variable "+variable.getId()+" esta duplicada", line);
		}
	}
	
	public void checkMethodDuplicate(MethodSymbol methodSymbol, ClassSymbol classParent, int line) {
		String key = methodSymbol.getType()+","+methodSymbol.getId()+","+classParent.getId();
		if(this.tableSymbol.getSymbol(key) != null) {
			agregarError("el metodo "+methodSymbol.getId()+" esta duplicado", line);
		}
	}
	
	public void checkMethodExist(MethodSymbol methodSymbol, int line) {
		ClassTable classParent = this.tableSymbol.getClassTable(methodSymbol.getParent().getId());
		Boolean cond = true;
		while(cond) {
			if(classParent == null) {
				agregarError("el metodo "+methodSymbol.getId()+ " no fue declarado", line);
				cond = false;
			}else {
				//String key = methodSymbol.getType()+","+methodSymbol.getId()+","+classParent.getId();
				if(this.tableSymbol.getMethodTableOfClassTable(classParent.getClassName(), methodSymbol.getId()) == null) {
					//String methodClassKey = classParent.getType()+","+classParent.getId();
					ClassTable methodClassTable = this.tableSymbol.getClassTable(classParent.getClassInheritName());
					if(methodClassTable == null || classParent.getClassName() == methodClassTable.getClassName()) {
						classParent = null;
					} else {
						classParent = methodClassTable;
					}
				} else {
					cond = false;
				}
			}
		}
	}
	
	public Boolean existClass(String className) {
		return this.tableSymbol.getClassTable().containsKey(className);
	}
	
	public Boolean existVarInClass(String className, String varNAme) {
		ClassTable classTable = this.tableSymbol.getClassTable().get(className);
		return classTable.getVariables().containsKey(varNAme);
	}
	
	public void checkParameterQtyForMehtod(MethodSymbol methodSymbol, int paramsQty, int line) {
		
		ClassTable classParent = this.tableSymbol.getClassTable(methodSymbol.getParent().getId());
		Boolean cond = true;
		while(cond) {
			if(classParent == null) {
				agregarError("el metodo "+methodSymbol.getId()+ " no fue declarado", line);
				cond = false;
			}else {
				MethodTable methodTable = this.tableSymbol.getMethodTableOfClassTable(classParent.getClassName(), methodSymbol.getId()); 
				if( methodTable == null) {
					ClassTable methodClassTable = this.tableSymbol.getClassTable(classParent.getClassInheritName());
					if(methodClassTable == null || classParent.getClassName() == methodClassTable.getClassName()) {
						classParent = null;
					} else {
						classParent = methodClassTable;
					}
				} else {
					if(methodTable.getParamsQty() != paramsQty) {
						classParent = null;
					}else {
						cond = false;
					}
				}
			}
		}
	}
	
	public void checkCorrectType(VariableSymbol variable, String returnedClass, int line) {
		ClassSymbol variableClass = variable.getVariableClass();
		ClassTable currentClassTable = this.tableSymbol.getClassTable(variableClass.getId());
		if(!currentClassTable.getClassName().equals(returnedClass)){
			agregarError("tipo de dato incorrecto. Esperado "+currentClassTable.getClassName()+" pasado "+returnedClass, line);
		}
	}
	
	public void agregarError(String message, int line) {
		this.errors.add(new Error(message, line));
	}
	
	public List<Error> getErrors(){
		return this.errors;
	}
	
	public void printErrors() {
		for(Error error : this.errors) {
			this.errorCount++;
			System.out.println(error.getFullMessage(this.errorCount));
		}
	}
	
	public void initClasses() {
		/*
		 * Clase Int
		 */
		ClassSymbol intSymbol = new ClassSymbol("Int", 0);
		ClassTable intTable = new ClassTable(intSymbol);
		
		/*
		 * Clase Bool
		 */
		ClassSymbol boolSymbol = new ClassSymbol("Bool", 0);
		ClassTable boolTable = new ClassTable(boolSymbol);
		
		/*
		 * Clase String
		 */
		ClassSymbol stringSymbol = new ClassSymbol("String", 0);
		ClassTable stringTable = new ClassTable(stringSymbol);
		//Se agregan los metodos de la clase String
		//length() : Int
		MethodSymbol lengthSymbol = new MethodSymbol("length", intSymbol, stringSymbol, 0);
		MethodTable lengthTable = new MethodTable(lengthSymbol);
		//concat(s : String) : String
		MethodSymbol concatSymbol = new MethodSymbol("concat", stringSymbol, stringSymbol, 0);
		MethodTable concatTable = new MethodTable(concatSymbol);
		ParameterSymbol concatParameter = new ParameterSymbol("s", stringSymbol, concatSymbol, 0);
		concatTable.addParameter(concatParameter);
		//substr(i : Int, l : Int) : String
		MethodSymbol substrSymbol = new MethodSymbol("substr", stringSymbol, stringSymbol, 0);
		MethodTable substrTable = new MethodTable(substrSymbol);
		ParameterSymbol substrParameter1 = new ParameterSymbol("i", intSymbol, substrSymbol, 0);
		substrTable.addParameter(substrParameter1);
		ParameterSymbol substrParameter2 = new ParameterSymbol("l", intSymbol, substrSymbol, 0);
		substrTable.addParameter(substrParameter2);
		stringTable.addMethod(lengthTable);
		stringTable.addMethod(concatTable);
		stringTable.addMethod(substrTable);
		
		/*
		 * Clase Object
		 */
		ClassSymbol objectSymbol = new ClassSymbol("Object", 0);
		ClassTable objectTable = new ClassTable(objectSymbol);
		//Se agregan los metodos de la clase Object
		//abort() : Object
		MethodSymbol abortSymbol = new MethodSymbol("abort", objectSymbol, objectSymbol, 0);
		MethodTable abortTable = new MethodTable(abortSymbol);
		//type_name() : String
		MethodSymbol typeNameSymbol = new MethodSymbol("type_name", stringSymbol, objectSymbol, 0);
		MethodTable typeNameTable = new MethodTable(typeNameSymbol);
		//copy() : SELF_TYPE
		MethodSymbol copySymbol = new MethodSymbol("copy", objectSymbol, objectSymbol, 0);
		MethodTable copyTable = new MethodTable(copySymbol);
		objectTable.addMethod(abortTable);
		objectTable.addMethod(typeNameTable);
		objectTable.addMethod(copyTable);
		
		/*
		 * Clase IO
		 */
		ClassSymbol ioSymbol = new ClassSymbol("IO", 0);
		ClassTable ioTable = new ClassTable(ioSymbol);
		//Se agregan los metodos de la clase Object
		//out_string(x : String) : SELF_TYPE
		MethodSymbol outStringSymbol = new MethodSymbol("out_string", ioSymbol, ioSymbol, 0);
		MethodTable outStringTable = new MethodTable(outStringSymbol);
		ParameterSymbol outStringParameter = new ParameterSymbol("x", stringSymbol, outStringSymbol, 0);
		outStringTable.addParameter(outStringParameter);
		//out_int(x : Int) : SELF_TYPE
		MethodSymbol outIntSymbol = new MethodSymbol("out_int", ioSymbol, ioSymbol, 0);
		MethodTable outIntTable = new MethodTable(outIntSymbol);
		ParameterSymbol outIntParameter = new ParameterSymbol("x", intSymbol, outIntSymbol, 0);
		outIntTable.addParameter(outIntParameter);
		//in_string() : String
		MethodSymbol inStringSymbol = new MethodSymbol("in_string", stringSymbol, ioSymbol, 0);
		MethodTable inStringTable = new MethodTable(inStringSymbol);
		//in_string() : String
		MethodSymbol inIntSymbol = new MethodSymbol("in_int", intSymbol, ioSymbol, 0);
		MethodTable inIntTable = new MethodTable(inIntSymbol);
		ioTable.addMethod(outStringTable);
		ioTable.addMethod(outIntTable);
		ioTable.addMethod(inStringTable);
		ioTable.addMethod(inIntTable);
		
		//claves para las clases
		String key = intSymbol.getType()+","+intSymbol.getId();
		this.tableSymbol.addSymbol(key, intSymbol);
		key = boolSymbol.getType()+","+boolSymbol.getId();
		this.tableSymbol.addSymbol(key, boolSymbol);
		key = stringSymbol.getType()+","+stringSymbol.getId();
		this.tableSymbol.addSymbol(key, stringSymbol);
		key = objectSymbol.getType()+","+objectSymbol.getId();
		this.tableSymbol.addSymbol(key, objectSymbol);
		key = ioSymbol.getType()+","+ioSymbol.getId();
		this.tableSymbol.addSymbol(key, ioSymbol);
		
		//claves para los metodos
		//metodos de Object
		key = abortSymbol.getType()+","+abortSymbol.getId()+","+objectSymbol.getId();
		this.tableSymbol.addSymbol(key, abortSymbol);
		key = typeNameSymbol.getType()+","+typeNameSymbol.getId()+","+objectSymbol.getId();
		this.tableSymbol.addSymbol(key, typeNameSymbol);
		key = copySymbol.getType()+","+copySymbol.getId()+","+objectSymbol.getId();
		this.tableSymbol.addSymbol(key, copySymbol);
		//metodos de String
		key = lengthSymbol.getType()+","+lengthSymbol.getId()+","+stringSymbol.getId();
		this.tableSymbol.addSymbol(key, lengthSymbol);
		key = concatSymbol.getType()+","+concatSymbol.getId()+","+stringSymbol.getId();
		this.tableSymbol.addSymbol(key, concatSymbol);
		key = substrSymbol.getType()+","+substrSymbol.getId()+","+stringSymbol.getId();
		this.tableSymbol.addSymbol(key, substrSymbol);
		//metods de IO
		key = outStringSymbol.getType()+","+outStringSymbol.getId()+","+ioSymbol.getId();
		this.tableSymbol.addSymbol(key, outStringSymbol);
		key = outIntSymbol.getType()+","+outIntSymbol.getId()+","+ioSymbol.getId();
		this.tableSymbol.addSymbol(key, outIntSymbol);
		key = inStringSymbol.getType()+","+inStringSymbol.getId()+","+ioSymbol.getId();
		this.tableSymbol.addSymbol(key, inStringSymbol);
		key = inIntSymbol.getType()+","+inIntSymbol.getId()+","+ioSymbol.getId();
		this.tableSymbol.addSymbol(key, inIntSymbol);
		
		//Se agregaron las clases a la tabla de simbolos
		this.tableSymbol.addClassTable(intTable);
		this.tableSymbol.addClassTable(boolTable);
		this.tableSymbol.addClassTable(stringTable);
		this.tableSymbol.addClassTable(objectTable);
		this.tableSymbol.addClassTable(ioTable);
	}
}
