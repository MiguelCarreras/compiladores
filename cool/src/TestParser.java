import Scanner.*;
import Parser.*;
import AST.*;
import AST.Visitor.*;
import java_cup.runtime.Symbol;
import java.util.*;

public class TestParser {
    public static void main(String [] args) {
        try {
            // create a scanner on the input file
            scanner s = new scanner(System.in);
            parser p = new parser(s);
            Symbol root;
	    // replace p.parse() with p.debug_parse() in next line to see trace of
	    // parser shift/reduce actions during parse
            root = p.parse();
            Program program = (Program)root.value;
            program.accept(new PrettyPrintVisitor());
            /*for (ClassDecl statement: program) {
                statement.accept(new PrettyPrintVisitor());
				System.out.print("\n");
            }*/
            System.out.print("\nParsing completed\n"); 
            
            TableSymbolVisitor symbolVisitor = new TableSymbolVisitor();
            program.accept(symbolVisitor);
            TypeCheckingVisitor typeCheckingVisitor = new TypeCheckingVisitor(symbolVisitor.getTableSymbol());
            program.accept(typeCheckingVisitor);
            
            if(typeCheckingVisitor.errorsSize() == 0) {
            	String dir= "SamplePrograms/JasminCode";
            	JasminVisitor jv = new JasminVisitor(dir, typeCheckingVisitor.getTableSymbol());
            	program.accept(jv);
                System.out.println("Generando Codigo Intermedio");
            }else {
            	typeCheckingVisitor.printErrors();
            }
            
        } catch (Exception e) {
            // yuck: some kind of error in the compiler implementation
            // that we're not expecting (a bug!)
            System.err.println("Unexpected internal compiler error: " + 
                               e.toString());
            // print out a stack dump
            e.printStackTrace();
        }
    }
}